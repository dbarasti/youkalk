package kalk;
import kalk.eccezioni.*;
import kalk.modelli.*;

import java.text.DecimalFormat;

/**
 * Created by dbara on 01/02/18.
 */
public class Esempio {

    public static void main (String[]args){
        try {
            System.out.println("CalcRC");
            ComponenteElettrico r1 = new Resistore(150);
            ComponenteElettrico r2 = new Resistore(200);

            r1.setTensioneDC(120);
            r1.normalizza();
            Resistore r1_cast = (Resistore) r1;
            r1_cast.setPotenzaMax(1);
            r1_cast.setCorrente();
            try {
                r1_cast.setPotenza();
            } catch (Alert a) {
                System.out.println(a.getMessage());
            }
            System.out.println("Resistore 1:");
            System.out.println(r1.valorePrincipaleString());
            System.out.println(r1_cast.correnteString());
            System.out.println(r1_cast.potenzaString());
            System.out.println(r1_cast.potenzaMaxString());
            System.out.println("");

            r2.setTensioneDC(12);
            r2.normalizza();
            Resistore r2_cast = (Resistore) r2;
            r2_cast.setPotenzaMax(1);
            r2_cast.setCorrente();
            try {
                r2_cast.setPotenza();
            } catch (Alert a) {
                System.out.println(a.getMessage());
            }
            System.out.println("Resistore 2:");
            System.out.println(r2.valorePrincipaleString());
            System.out.println(r2_cast.correnteString());
            System.out.println(r2_cast.potenzaString());
            System.out.println(r2_cast.potenzaMaxString());

            ComponenteElettrico r3 = r1.serie(r2);
            System.out.println("\nResistore 1 -- Resistore 2");
            System.out.println(r3.valorePrincipaleString() + " " + ((Resistore)r3).potenzaMaxString());

            ComponenteElettrico r4 = r3.parallelo(r2);
            System.out.println("Resistore 3 // Resistore 2");
            System.out.println(r4.valorePrincipaleString() + " " + ((Resistore)r4).potenzaMaxString());



            ComponenteElettrico c1 = new Condensatore(0.5);
            ComponenteElettrico c2 = new Condensatore(0.8);
            c1.setTensioneDC(120);
            c1.normalizza();
            Condensatore c1_cast = (Condensatore) c1;
            c1_cast.setResistenza(5000);
            c1_cast.setT_carica();

            System.out.println("\nCondensatore 1:");
            System.out.println(c1.valorePrincipaleString());
            System.out.println(c1_cast.tensioneDCString());
            System.out.println(c1_cast.resistenzaString());
            System.out.println(c1_cast.t_caricaString());

            c2.setTensioneDC(150);
            c2.normalizza();
            Condensatore c2_cast = (Condensatore) c2;
            c2_cast.setResistenza(1000);
            c2_cast.setT_carica();

            System.out.println("Condensatore 2:");
            System.out.println(c2.valorePrincipaleString());
            System.out.println(c2_cast.tensioneDCString());
            System.out.println(c2_cast.resistenzaString());
            System.out.println(c2_cast.t_caricaString());


            ComponenteElettrico c3 = c1.serie(c2);
            System.out.println("\nCondensatore 1 -- Condensatore 2");
            System.out.println(c3.valorePrincipaleString());

            ComponenteElettrico c4 = c3.parallelo(c2);
            System.out.println("Condensatore 3 // Condensatore 2");
            System.out.println(c4.valorePrincipaleString());


            ComponenteElettrico I1 = new Induttore(54);
            ComponenteElettrico I2 = new Induttore(85);
            I1.setTensioneAC(120);
            I1.setFrequenza(1000);
            //I1.normalizza();
            Induttore I1_cast = (Induttore) I1;
            I1_cast.setResistenza(5000);
            try {
                I1_cast.setCorrente();
            }catch(BadOperation badOp){
                badOp.getMessage();
            }
            DecimalFormat df = new DecimalFormat("#.###");

            System.out.println("\nInduttore 1:");
            System.out.println(I1.valorePrincipaleString());
            System.out.println(I1_cast.tensioneACString() + " " + I1_cast.frequenzaString() + " " + I1_cast.rmsString());
            System.out.println("ω: " + df.format(I1_cast.getPulsazione()));
            System.out.println("tg(φ): " + df.format(I1_cast.getSfasamento()));
            System.out.println(I1_cast.correnteString());

            I2.setTensioneAC(220);
            I2.setFrequenza(5000);
            //I1.normalizza();
            Induttore I2_cast = (Induttore) I2;
            I2_cast.setResistenza(2500);
            try {
                I2_cast.setCorrente();
            }catch(BadOperation badOp){
                badOp.getMessage();
            }


            System.out.println("Induttore 2:");
            System.out.println(I2.valorePrincipaleString());
            System.out.println(I2_cast.tensioneACString() + " " + I2_cast.frequenzaString() + " " + I2_cast.rmsString());
            System.out.println("ω: " + df.format(I2_cast.getPulsazione()));
            System.out.println("tg(φ): " + df.format(I2_cast.getSfasamento()));
            System.out.println(I2_cast.correnteString());

            ComponenteElettrico I3 = I1.serie(I2);
            System.out.println("\nInduttore 1 -- Induttore 2");
            System.out.println(I3.valorePrincipaleString());

            ComponenteElettrico I4 = I1.parallelo(I2);
            System.out.println("\nInduttore 1 // Induttore 2");
            System.out.println(I3.valorePrincipaleString());

        }
        catch(BadOperation e){
            System.out.println(e.getMessage());
        }
    }
}
