package kalk.modelli;
import kalk.eccezioni.*;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Created by dbara on 01/02/18.
 */
public class Resistore extends ComponenteElettrico{

    private Misura potenzaMax; //potenza massima dissipabile dal resistore
    private Misura corrente;
    private Misura potenza;

    public Resistore() throws BadOperation {
        this(0);
    }

    public Resistore(double v) throws BadOperation {
        super(v, "ohm");
        potenzaMax = new Misura(0,"W");
        corrente = new Misura(0, "A");
        potenza = new Misura(0,"W");
    }

    public Resistore(Resistore r) {
        super(r);
        potenzaMax = new Misura(r.potenzaMax);
        corrente = new Misura(r.corrente);
        potenza = new Misura(r.potenza);
    }

    public double potenza_max_equivalente_par(ComponenteElettrico r2, double val) {
        double wmax = 0, vmax = 0;
        double r1_valore_assoluto = getValorePrincipale();
        Resistore r2_cast = (Resistore)r2;
        double r2_valore_assoluto =  r2.getValorePrincipale();
        if(r1_valore_assoluto * getPotenzaMax() < r2_valore_assoluto * r2_cast.getPotenzaMax()){
            vmax = sqrt(r1_valore_assoluto * getPotenzaMax());
        }
        else
            vmax = sqrt(r2_valore_assoluto * r2_cast.getPotenzaMax());
        wmax = pow(vmax, 2) / val;
        return wmax;
    }

    public double potenza_max_equivalente_ser(ComponenteElettrico r2, double val) {
        double wmax = 0, imax = 0;
        double r1_valore_assoluto = getValorePrincipale();
        Resistore r2_cast = (Resistore)r2;
        double r2_valore_assoluto = r2.getValorePrincipale();
        if( getPotenzaMax() / r1_valore_assoluto < ( r2_cast.getPotenzaMax() / r2_valore_assoluto ))
            imax = sqrt( getPotenzaMax() / r1_valore_assoluto );
        else
            imax = sqrt(r2_cast.getPotenzaMax() / r2_valore_assoluto);
        wmax = pow(imax, 2) * val;
        return wmax;
    }

    public Resistore copy() {
        return new Resistore(this);
    }

    public Resistore parallelo(ComponenteElettrico r) throws BadOperation {
        double valore1 = getValorePrincipale();
        double valore2 = r.getValorePrincipale();
        if(valore1 > 0 && valore2 > 0){
            double valore_par = (valore1 * valore2) / (valore1 + valore2);
            Resistore aux = new Resistore(valore_par);
            aux.setPotenzaMax( potenza_max_equivalente_par(r, valore_par) );
            aux.potenzaMax.normalizza();
            aux.valore_principale.normalizza();
            return aux;
        }
        else{
            throw new BadOperation("Uno degli operandi non è stato definito");
        }
    }

    public Resistore serie(ComponenteElettrico r) throws BadOperation {
        double valore1 = getValorePrincipale();
        double valore2 = r.getValorePrincipale();
        if(valore1 > 0 && valore2 > 0){
            double valore_serie = valore1 + valore2;
            Resistore aux = new Resistore(valore_serie);
            aux.setPotenzaMax(potenza_max_equivalente_ser(r, valore_serie));
            aux.potenzaMax.normalizza();
            aux.valore_principale.normalizza();
            return aux;
        }
        else
            throw new BadOperation("Uno degli operandi non è stato definito");
    }

    public void setCorrente() throws BadOperation {
        if(getTensioneDC() > 0){
            double curr = getTensioneDC() / getValorePrincipale();
            corrente.setValore(curr);
            corrente.setPrefisso(' ');
            corrente.normalizza();
        }
        else{
            throw new BadOperation("Valore tensioneDC non specificato");
        }
    }

    public double getCorrente() {
        return corrente.getValore() * corrente.getMoltiplicatore();
    }

    public double getPotenzaMax() {
        return potenzaMax.getValore() * potenzaMax.getMoltiplicatore();
    }

    public void setPotenzaMax(double value) throws BadOperation {
        potenzaMax.setValore(value);
        potenzaMax.setPrefisso(' ');
        potenzaMax.normalizza();
    }

    public double getPotenza() {
        return potenza.getValore() * potenza.getMoltiplicatore();
    }

    public void setPotenza() throws BadOperation, Alert {
        double potenzadissipata = getTensioneDC()* getCorrente();
        potenza.setValore(potenzadissipata);
        potenza.setPrefisso(' ');
        potenza.normalizza();
        if(getPotenzaMax() > 0 && getPotenzaMax() < potenzadissipata)
            throw new Alert("Attenzione: la potenza massima è stata superata");
    }

    public String potenzaMaxString() {
        return potenzaMax.toString();
    }

    public String correnteString() {
        return corrente.toString();
    }

    public String potenzaString() {
        return potenza.toString();
    }

    public void reset() {
        super.reset();
        try {
            potenzaMax.reset();
            corrente.reset();
            potenza.reset();
        } catch (BadOperation badOperation) {
            badOperation.printStackTrace();
        }
    }
}
