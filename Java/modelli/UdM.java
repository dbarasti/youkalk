package kalk.modelli;
import java.util.ArrayList;
import kalk.eccezioni.*;
/**
 * Created by dbara on 01/02/18.
 */
class UdM {
    private String unita;
    private int index;

    public UdM(UdM u) {
        unita = u.unita;
        index = u.index;
    }

    public UdM(String unit, Character pref){
        unita = unit;
        try {
            setPrefisso(pref);
        } catch (BadOperation badOperation) {
            badOperation.printStackTrace();
        }
    }

    public UdM(String unit){
        this(unit, ' ');
    }

    public static ArrayList<Character> prefissi;

    static {
        prefissi = new ArrayList<Character>();
        prefissi.add('n');
        prefissi.add('u');
        prefissi.add('m');
        prefissi.add(' ');
        prefissi.add('K');
        prefissi.add('M');
    }

    public static int offset = prefissi.indexOf(' '); //in base al numero di elementi in 'prefissi'

    public String getUnita() {
        return unita;
    }

    public void setUnita(String value) {
        unita = value;
    }

    public double getMoltiplicatore() {
        return Math.pow(10, (index - offset) * 3);
    }

    public Character getPrefisso() {
        return prefissi.get(index);
    }

    public void setPrefisso(Character value) throws BadOperation {
        int indice = prefissi.indexOf(value);
        if(indice == -1){
            throw new BadOperation("Prefisso non esistente");
        }else
            index = indice;
    }

    public Boolean next() {
        if(getPrefisso() != prefissi.get(prefissi.size() - 1)){
            ++index;
            return true;
        }
        return false;
    }

    public Boolean prev() {
        if(getPrefisso() != prefissi.get(0)){
            --index;
            return true;
        }
        return false;
    }
}
