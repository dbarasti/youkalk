package kalk.modelli;

import kalk.eccezioni.*;

/**
 * Created by dbara on 01/02/18.
 */
public abstract class ComponenteElettrico {
    protected Misura valore_principale;
    protected Misura tensioneDC;
    protected MisuraTA tensioneAC;

    public ComponenteElettrico(double v, String u){
        valore_principale = new Misura(v, u);
        tensioneDC = new Misura(0, "V");
        tensioneAC = new MisuraTA(0, 0);
    }

    public ComponenteElettrico(ComponenteElettrico r) {
        valore_principale = new Misura(r.valore_principale);
        tensioneDC = new Misura(r.tensioneDC);
        tensioneAC = new MisuraTA(r.tensioneAC);
    }

    public abstract ComponenteElettrico copy();
    public abstract ComponenteElettrico parallelo(ComponenteElettrico c) throws BadOperation;
    public abstract ComponenteElettrico serie(ComponenteElettrico c) throws BadOperation;

    public void reset() {
        try {
            valore_principale.reset();
            tensioneDC.reset();
        } catch (BadOperation badOperation) {
            badOperation.printStackTrace();
        }
    }

    public void setValorePrincipale(double value) {
        valore_principale.setValore(value);
    }

    public double getValorePrincipale() {
        return valore_principale.getValore() * valore_principale.getMoltiplicatore();
    }

    public double getTensioneDC() {
        return tensioneDC.getValore() * tensioneDC.getMoltiplicatore();
    }

    public void setTensioneDC(double value) throws BadOperation {
        tensioneDC.setValore(value);
        tensioneDC.setPrefisso(' ');
        tensioneDC.normalizza();
    }

    public String tensioneDCString() {
        return tensioneDC.toString();
    }

    double getTensioneAC(){
        return tensioneAC.getValore() * tensioneAC.getMoltiplicatore();
    }

    public void setTensioneAC(double value)
    {
        tensioneAC.setValore(value);
    }

    public String tensioneACString()
    {
        return tensioneAC.toString();
    }

    public double getFrequenza()
    {
        return tensioneAC.getFrequenza();
    }

    public void setFrequenza(double val)
    {
        tensioneAC.setFrequenza(val);
    }

    public String frequenzaString()
    {
        return tensioneAC.frequenzaString();
    }

    public double getRMS()
    {
        return tensioneAC.getRMS();
    }

    public void setRMS()
    {
        tensioneAC.setRMS();
    }

    public String rmsString()
    {
        return tensioneAC.rmsString();
    }

    public void setUnitaPrincipale(Character value) throws BadOperation {
        valore_principale.setPrefisso(value);
    }

    public String valorePrincipaleString() {
        return valore_principale.toString();
    }

    public void normalizza() {
        valore_principale.normalizza();
    }
}
