package kalk.modelli;

import kalk.eccezioni.BadOperation;

import static java.lang.Math.pow;
import static java.lang.StrictMath.sqrt;

/**
 * Created by dbara on 13/02/18.
 */
public class Induttore extends Solenoide {
    private Misura corrente;
    private Misura resistenza;

    public Induttore(double val, double l, int n){
        super(val, l, n);
        resistenza = new Misura(9, "ohm");
        corrente = new Misura(0, "A");
    }
    public Induttore(double val){
        this(val, 0, 0);
    }
    public Induttore(Induttore i){
        super(i);
        corrente = new Misura(i.corrente);
        resistenza = new Misura(i.resistenza);
    }

    public double getResistenza()
    {
        return resistenza.getValore() * resistenza.getMoltiplicatore();
    }

    public void setResistenza(double v)
    {
        resistenza.setValore(v);
        resistenza.setPrefisso(' ');
        resistenza.normalizza();
    }

    public String resistenzaString()
    {
        return resistenza.toString();
    }

    public double getCorrente()
    {
        return corrente.getValore() * corrente.getMoltiplicatore();
    }

    public void setCorrente() throws BadOperation {
        if(getTensioneAC() > 0){
            double curr = getRMS() / sqrt(pow(getResistenza(),2) + pow(getPulsazione()*getValorePrincipale(),2));
            corrente.setValore(curr);
            corrente.setPrefisso(' ');
            corrente.normalizza();
        }
        else{
            throw new BadOperation("Valore tensione non specificato");
        }
    }

    public String correnteString(){
        return corrente.toString();
    }

    public double getPulsazione()
    {
        return tensioneAC.getW();
    }

    public double getSfasamento()
    {
        if(getResistenza() > 0)
            return (getPulsazione() * getValorePrincipale()) / getResistenza();
        else return 0;
    }

    public Induttore parallelo(ComponenteElettrico i) throws BadOperation {
        double valore1 = getValorePrincipale();
        double valore2 = i.getValorePrincipale();
        if(valore1 > 0 && valore2 > 0){
            double valore_par = (valore1 * valore2) / (valore1 + valore2);
            Induttore aux = new Induttore(valore_par);
            aux.valore_principale.normalizza();
            return aux;
        }
        else{
            throw new BadOperation("Uno degli operandi non è stato definito");
        }
    }

    public Induttore serie(ComponenteElettrico i) throws BadOperation {
        double valore1 = getValorePrincipale();
        double valore2 = i.getValorePrincipale();
        if(valore1 > 0 && valore2 > 0){
            double valore_serie = valore1 + valore2;
            Induttore aux = new Induttore(valore_serie);
            aux.valore_principale.normalizza();
            return aux;
        }
        else
            throw new BadOperation("Uno degli operandi non è stato definito");
    }

    public Induttore copy()
    {
        return new Induttore(this);
    }

    public void reset()
    {
        super.reset();
        try {
            resistenza.reset();
            corrente.reset();
        }
        catch (BadOperation badOperation) {
            badOperation.printStackTrace();
        }

    }

}
