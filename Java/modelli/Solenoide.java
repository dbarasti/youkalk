package kalk.modelli;

import kalk.eccezioni.BadOperation;

/**
 * Created by dbara on 13/02/18.
 */
public abstract class Solenoide extends ComponenteElettrico {
    protected Misura lunghezza;
    protected int Nspire;

    public Solenoide(double val, double l, int n) {
        super(val, "H");
        lunghezza = new Misura(l, "m");
        Nspire = n;
        valore_principale.setPrefisso('m');
    }
    public Solenoide(Solenoide i){
        super(i);
        lunghezza = i.lunghezza;
        Nspire = i.Nspire;
    }

    public double getLunghezza()
    {
        return lunghezza.getValore() * lunghezza.getMoltiplicatore();
    }

    public void setLunghezza(Misura value)
    {
        lunghezza = value;
        lunghezza.setPrefisso(' ');
        lunghezza.normalizza();
    }

    public int getNspire()
    {
        return Nspire;
    }

    public void setNspire(int value)
    {
        Nspire = value;
    }

    public void reset()
    {
        try {
            super.reset();
            lunghezza.reset();
        }
        catch(BadOperation badOperation) {
            badOperation.printStackTrace();
        }
        Nspire = 0;
    }



}
