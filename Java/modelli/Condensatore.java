package kalk.modelli;
import kalk.eccezioni.*;
/**
 * Created by dbara on 02/02/18.
 */
public class Condensatore extends ComponenteElettrico {
    private Misura t_carica;
    private Misura resistenza; //resistenza che viene usata per il calcolo del t_carica

    public Condensatore(double v) throws BadOperation {
        super(v, "F");
        t_carica = new Misura(0, "S");
        resistenza = new Misura(5000, "ohm");
        resistenza.normalizza();
        valore_principale.setPrefisso('m');
    }

    public Condensatore(Condensatore c) {
        super(c);
        t_carica = c.t_carica;
        resistenza = c.resistenza;
    }

    public Condensatore copy() {
        return new Condensatore(this);
    }

    public Condensatore parallelo(ComponenteElettrico c) throws BadOperation {
        double valore1 = getValorePrincipale();
        double valore2 = c.getValorePrincipale();
        if(valore1 > 0.0 && valore2 > 0.0){
            double valore_aux = valore1 + valore2;
            Condensatore aux = new Condensatore(valore_aux); //magari metto direttamente val1+val2?
            aux.valore_principale.normalizza(); //sistemare normalizzazione
            return aux;
        }
        throw new BadOperation("Uno degli operandi non è stato definito");
    }

    public Condensatore serie(ComponenteElettrico c ) throws BadOperation {
        double valore1 = getValorePrincipale();
        double valore2 = c.getValorePrincipale();
        if(valore1 > 0 && valore2 > 0.0){
            double valore_aux = (valore1 * valore2) / (valore1 + valore2);
            Condensatore aux = new Condensatore(valore_aux);
            aux.valore_principale.normalizza(); //da sistemare normalizzazione
            return aux;
        }
        else
            throw new BadOperation("Uno degli operandi non è stato definito");
    }

    //calcola tensioneDC ai capi del condensatore in un istante t:
    public double V(double t) //calcola la tensioneDC ai capi del condensatore nel tempo t
    {
        double tensione = getTensioneDC();
        double RC = getResistenza() * getValorePrincipale();
        if(getT_carica() > 0){
            return tensione*(1 - Math.pow(Math.E, -t/RC));
        }
        else
            return 0;
    }

    public void setResistenza(double v) throws BadOperation {
        resistenza.setValore(v);
        resistenza.setPrefisso(' ');
        resistenza.normalizza();
    }

    public String resistenzaString()
    {
        return resistenza.toString();
    }

    public double getResistenza() {
        return resistenza.getValore() * resistenza.getMoltiplicatore();
    }

    public void setT_carica() throws BadOperation {
        if(getTensioneDC() > 0){
            double t_c = 5 * getResistenza() * getValorePrincipale();
            t_carica.setValore(t_c);
            t_carica.setPrefisso(' ');
            t_carica.normalizza();
        }
        else
            throw new BadOperation("Valore tensioneDC mancante");
    }

    public double getT_carica() {
        return t_carica.getValore() * t_carica.getMoltiplicatore();
    }

    public String t_caricaString() {
        return t_carica.toString();
    }

    public void reset(){
        super.reset();
        try {
            t_carica.reset();
        } catch (BadOperation badOperation) {
            badOperation.printStackTrace();
        }
    }
}
