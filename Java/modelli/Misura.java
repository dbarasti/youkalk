package kalk.modelli;
import kalk.eccezioni.*;

import java.text.DecimalFormat;

import static jdk.nashorn.internal.objects.NativeString.trim;

/**
 * Created by dbara on 01/02/18.
 */
public class Misura {
    protected double valore = 0;
    protected UdM unita;

    public Misura(double val, String unit){
        valore = val;
        unita = new UdM(unit);
    }

    public Misura(Misura m) {
        valore = m.valore;
        unita = m.unita;
    }

    public void normalizza() {
        while(valore>=1000 && unita.next()) {
            valore /= 1000;
        }

        while(valore != 0 && valore <= 0.1 && unita.prev()) {
            valore *= 1000;
        }
    }

    public String getUnita() {
        return unita.getUnita();
    }

    public void setUnita(String u) {
        unita.setUnita(u);
    }

    public String toString() {
        DecimalFormat df = new DecimalFormat("#.###");
        return df.format(valore) + trim(unita.getPrefisso()) + unita.getUnita();
    }

    public void setPrefisso(Character value){
        try {
            unita.setPrefisso(value);
        } catch (BadOperation badOperation) {
            badOperation.printStackTrace();
        }
    }

    Character getPrefisso() {
        return unita.getPrefisso();
    }

    public double getValore() {
        return valore;
    }

    public void setValore(double value){
        valore = value;
    }

    public double getMoltiplicatore() {
        return unita.getMoltiplicatore();
    }

    public void reset() throws BadOperation {
        valore = 0;
        unita.setPrefisso(' ');
    }
}
