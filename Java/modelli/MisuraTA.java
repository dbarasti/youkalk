package kalk.modelli;

import kalk.eccezioni.BadOperation;

import static java.lang.Math.PI;
import static java.lang.Math.sqrt;

/**
 * Created by dbara on 13/02/18.
 */
public class MisuraTA extends Misura {
    private Misura frequenza;
    private Misura RMS;
    public MisuraTA(double V, double f){
        super(V, "V");
        frequenza = new Misura(f, "Hz");
        RMS = new Misura(valore/sqrt(2), "V");
    }
    public MisuraTA(MisuraTA m){
        super(m);
        frequenza = m.frequenza;
        RMS = m.RMS;
    }
    public void setValore(double value){
        valore = value;
        setPrefisso(' ');
        normalizza();
        setRMS();
    }

    public double getFrequenza()
    {
        return frequenza.getValore() * frequenza.getMoltiplicatore();
    }

    public void setFrequenza(double val){
        frequenza.setValore(val);
        frequenza.setPrefisso(' ');
        frequenza.normalizza();
    }

    public String frequenzaString()
    {
        return frequenza.toString();
    }

    public double getRMS()
    {
        return RMS.getValore() * RMS.getMoltiplicatore();
    }

    public void setRMS(){
        RMS.setValore(valore/sqrt(2));
    }

    public String rmsString()
    {
        return RMS.toString();
    }

    public double getW()
    {
        return 2 * PI* getFrequenza();
    }
}
