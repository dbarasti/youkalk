package kalk.eccezioni;
/**
 * Created by dbara on 01/02/18.
 */
public class BadOperation extends Exception {
    public BadOperation(String e) {
        super(e);
    }
}

