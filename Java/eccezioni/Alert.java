package kalk.eccezioni;

/**
 * Created by dbara on 04/02/18.
 */
public class Alert extends Exception {
    public Alert(String e){
        super(e);
    }
}
